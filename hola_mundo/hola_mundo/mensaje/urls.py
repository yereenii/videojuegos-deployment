from django.urls import path
from mensaje import views

urlpatterns = [
    path('', views.hola_mundo),
    path('mensaje', views.mensaje),
    path('mensaje/<int:edad>/<str:nombre>', views.mensaje_variables),
]

# hola/
# hola/mensaje
# hola/hola
#     Lista de materias
#     Álgebra 10
#     SO Linux 8
    
# [
#     {'materia': 'Álgenra','calificacion':10},
#     {}
# ]